<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Задача №3</title>
</head>
<body>
<div>
    <nav>
        <ul>
            <li>
                <a href="/">На главную</a>
            </li>
            <li>
                <a href="/task_1/">Задача №1</a>
            </li>
            <li>
                <a href="/task_2/">Задача №2</a>
            </li>
            <li>
                <a href="/task_3/">Задача №3</a>
            </li>
        </ul>
    </nav>
</div>
<?php
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

$servername = "mysql";
$username = "user";
$password = "userpass";
$database = "mydb";

// Создаем соединение
try {
    $mysqli = new mysqli($servername, $username, $password, $database);
    $query = 'SELECT * FROM countries;';
    $query .= 'SELECT * FROM cities;';
    $query .= 'SELECT * FROM animal_classes;';
    $query .= 'SELECT * FROM animals;';

    $mysqli->set_charset('utf8mb4');
    $mysqli->multi_query($query);
    do {
        if ($result = $mysqli->store_result()) {
            while ($row = $result->fetch_row()) {
                foreach ($row as $item) {
                    echo " | $item" ;
                }
                echo " |<br/>";
            }
        }
        if ($mysqli->more_results()) {
            echo "-----------------<br/>";
        }
        $result->free();
    } while ($mysqli->next_result());
    // Закрываем соединение
    $mysqli->close();
} catch (Exception $e) {
    die("Ошибка соединения: " . $e->getMessage());
}
?>
</body>
</html>