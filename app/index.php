<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>crt-symfony-2</title>
</head>
<body>
<div>
    <nav>
        <ul>
            <li>
                <a href="/">На главную</a>
            </li>
            <li>
                <a href="/task_1/">Задача №1</a>
            </li>
            <li>
                <a href="/task_2/">Задача №2</a>
            </li>
            <li>
                <a href="/task_3/">Задача №3</a>
            </li>
        </ul>
    </nav>
</div>
</body>
</html>
