<?php

/**
 * Товар
 */
class Product
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var int
     */
    protected int $price;

    public function __construct(string $name, int $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * Возвращает наименование товара
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Возвращает стоимость товара
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }
}