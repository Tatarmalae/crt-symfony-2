<?php

use JetBrains\PhpStorm\Pure;

/**
 * Позиция одного товара в корзине
 */
class BasketPosition
{
    /**
     * @var Product
     */
    protected Product $product;

    /**
     * @var int
     */
    protected int $quantity;

    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * Возвращает наименование товара в этой позиции
     * @return string
     */
    #[Pure] public function getProduct(): string
    {
        return $this->product->getName();
    }

    /**
     * Возвращает количество товаров в этой позиции
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Возвращает стоимость позиции
     * @return int
     */
    #[Pure] public function getPrice(): int
    {
        return $this->product->getPrice();
    }
}