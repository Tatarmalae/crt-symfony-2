<?php

/**
 * Заказ товара
 */
class Order
{
    /**
     * @var Basket
     */
    protected Basket $basket;

    /**
     * @var int
     */
    protected int $delivery;

    public function __construct(Basket $basket, int $delivery)
    {
        $this->basket = $basket;
        $this->delivery = $delivery;
    }

    /**
     * Возвращает объект-корзину, которая хранится в заказе
     * @return Basket
     */
    public function getBasket(): Basket
    {
        return $this->basket;
    }

    /**
     * Возвращает общую стоимость заказа с учетом доставки
     * @return int
     */
    public function getPrice(): int
    {
        $basket = $this->getBasket();
        return $basket->getPrice() + $this->delivery;
    }
}