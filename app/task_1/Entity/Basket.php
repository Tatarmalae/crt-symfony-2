<?php

/**
 * Корзина
 */
class Basket
{
    /**
     * @var array
     */
    protected array $products;

    public function __construct()
    {
        $this->products = [];
    }

    /**
     * Создаёт новую позицию и добавляет её в корзину
     * @param Product $product
     * @param int $quantity
     * @return BasketPosition
     */
    public function addProduct(Product $product, int $quantity): BasketPosition
    {
        $newBasketPosition = new BasketPosition($product, $quantity);
        array_push($this->products, $newBasketPosition);

        return $newBasketPosition;
    }

    /**
     * Возвращает стоимость всех позиций в корзине с учетом кол-ва
     * @return int
     */
    public function getPrice(): int
    {
        $total = 0;

        /** @var BasketPosition $product */
        foreach ($this->products as $product) {
            $total = $total + $product->getPrice() * $product->getQuantity();
        }

        return $total;
    }

    /**
     * Возвращает информацию о корзине в виде строки
     * <Наименование товара> - <Цена одной позиции> - <Количество>
     * @return string
     */
    public function describe(): string
    {
        $res = '';

        /** @var BasketPosition $product */
        foreach ($this->products as $product) {
            $res .= $product->getProduct() . ' - ' . $product->getPrice() . ' - ' . $product->getQuantity() . '<br>';
        }
        return $res;
    }
}