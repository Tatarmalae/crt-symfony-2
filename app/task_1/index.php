<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Задача №1</title>
</head>
<body>
<div>
    <nav>
        <ul>
            <li>
                <a href="/">На главную</a>
            </li>
            <li>
                <a href="/task_1/">Задача №1</a>
            </li>
            <li>
                <a href="/task_2/">Задача №2</a>
            </li>
            <li>
                <a href="/task_3/">Задача №3</a>
            </li>
        </ul>
    </nav>
</div>
<?php
require_once 'Entity/Order.php';
require_once 'Entity/Basket.php';
require_once 'Entity/BasketPosition.php';
require_once 'Entity/Product.php';

//Создадим товары
$book = new Product('Book', 500);
$coffee = new Product('Coffee', 100);
$spotify = new Product('Spotify', 300);

//Добавим товары в корзину
$basket = new Basket();
$basket->addProduct($book, 1);
$basket->addProduct($coffee, 20);
$basket->addProduct($spotify, 2);

//Сформируем заказ из корзины
$order = new Order($basket, 300);

//Выведем информацию о заказе в виде
//"Заказ, на сумму: <сумма заказа> Состав: <информация о корзине>"
echo "Заказ, на сумму: " . $order->getPrice() . "<br/><br/>Состав:<br/>" . $basket->describe();

?>
</body>
</html>