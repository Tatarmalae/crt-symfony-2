<?php

/**
 * Класс, реализующий валидацию данных формы
 */
class UserFormValidator
{

    /**
     * Метод осуществляет валидацию данных
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function validate(array $data): bool
    {
        if (empty($data['name'])) {
            throw new Exception("Имя не должно быть пустым");
        }

        if ($data['age'] < 18) {
            throw new Exception("Вам уже должно быть 18");
        }

        if (empty($data['email'])) {
            throw new Exception("Email не должен быть пустым");
        }

        $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Введите правильный Email");
        }

        return true;
    }
}