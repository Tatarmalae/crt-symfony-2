<?php

/**
 * Модель пользователя
 * Класс лишь имитирует взаимодействие с базой данных
 */
class User
{

    /**
     * Данные пользователя
     * @var array
     */
    private array $user = [];

    /**
     * Загрузка пользователя по $id
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function load(int $id): bool
    {
        if (!$id || $id > 12) {
            throw new Exception('Пользователь не найден');
        }

        return true;
    }

    /**
     * Сохранение пользователя
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function save(array $data): string
    {
        if (!rand(0, 1)) {
            array_push($this->user, $data);
            throw new Exception('Не удалось добавить пользователя');
        }

        return 'Пользователь успешно добавлен';
    }
}