<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Задача №2</title>
</head>
<body>
<div>
    <nav>
        <ul>
            <li>
                <a href="/">На главную</a>
            </li>
            <li>
                <a href="/task_1/">Задача №1</a>
            </li>
            <li>
                <a href="/task_2/">Задача №2</a>
            </li>
            <li>
                <a href="/task_3/">Задача №3</a>
            </li>
        </ul>
    </nav>
</div>
<form method="POST" action="index.php">
    <div>
        <label for="id">Введите ID</label>
        <input type="number" name="id" id="id" value="<?= $_POST['id'] ?? 0 ?>">
    </div>
    <div>
        <label for="name">Ваше имя</label>
        <input type="text" name="name" id="name" value="<?= $_POST['name'] ?? false ?>">
    </div>
    <div>
        <label for="age">Ваш возраст</label>
        <input type="text" name="age" id="age" value="<?= $_POST['age'] ?? false ?>">
    </div>
    <div>
        <label for="email">Ваш Email</label>
        <input type="text" name="email" id="email" value="<?= $_POST['email'] ?? false ?>">
    </div>
    <button type="submit">Отправить</button>
</form>
<?php
require_once 'Entity/User.php';
require_once 'Entity/UserFormValidator.php';

if ($_POST) {
    $user = new User();
    $data = [
        'id' => $_POST['id'],
        'name' => $_POST['name'],
        'age' => $_POST['age'],
        'email' => $_POST['email'],
    ];

    try {
        $user->load($_POST['id']);
        $userValidate = new UserFormValidator();
        $userValidate->validate($data);
        echo $user->save($data);
        echo '<meta http-equiv="refresh" content="0;URL=index.php" />';
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
$user = new User();
?>
</body>
</html>